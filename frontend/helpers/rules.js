const rules = {
  required (value) {
    return (
      (value !== null &&
        value !== undefined &&
        value !== '' &&
        value !== false) ||
      'Campo obrigatório'
    )
  }
}

export default rules
