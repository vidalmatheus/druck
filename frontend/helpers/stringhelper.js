export default {
  removeAccents (str) {
    if (!str) {
      return str
    }
    const accents =
      'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž'
    const accentsOut =
      'AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz'
    str = str.split('')
    str.forEach((letter, index) => {
      const i = accents.indexOf(letter)
      if (i !== -1) {
        str[index] = accentsOut[i]
      }
    })
    return str.join('')
  },
  search (query, text) {
    text = text || ''
    if (!query) {
      return 0
    }
    const queryWords = this.splitAndCleanText(query)
    const textWords = this.splitAndCleanText(text)
    let matchRank = 0.0
    queryWords.forEach(w1 => {
      let wordRank = 0.0
      textWords.forEach(w2 => {
        if (w1 === w2) {
          wordRank += 1
        } else if (w2.indexOf(w1) === 0) {
          wordRank += 0.7
        } else if (w2.includes(w1)) {
          wordRank += 0.2
        }
      })
      matchRank += wordRank || -0.4
    })
    return matchRank / (textWords.filter(a => a.length > 2).length || 1)
  },
  splitAndCleanText (text) {
    return (
      this.removeAccents(text)
        .toLowerCase()
        .match(/[a-z0-9\s]/g) || []
    )
      .join('')
      .split(/\s+/)
      .filter(a => !!a)
  }
}
