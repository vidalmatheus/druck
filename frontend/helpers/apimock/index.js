import auth from './auth'
import aparelhos from './aparelhos'

export default {
  auth,
  aparelhos
}
