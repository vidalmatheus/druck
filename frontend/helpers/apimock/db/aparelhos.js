export const aparelhos = [
  {
    id: 1,
    tipo: 'Microondas',
    marca: 'Brastemp',
    modelo: 'X',
    potencia: '300W',
    eficiencia: 'B',
    ano: '2021',
    consumo: '25-50'
  },
  {
    id: 2,
    tipo: 'Liquidificador',
    marca: 'Consul',
    modelo: 'Y',
    potencia: '200W',
    eficiencia: 'B',
    ano: '2019',
    consumo: '25-50'
  },
  {
    id: 3,
    tipo: 'Geladeira',
    marca: 'Consul',
    modelo: 'Z',
    potencia: '600W',
    eficiencia: 'B',
    ano: '2020',
    consumo: '100-125'
  }
]

export const tipos = [
  {
    id: 1,
    name: 'Microondas'
  },
  {
    id: 2,
    name: 'Chuveiro elétrico'
  },
  {
    id: 3,
    name: 'Liquidificador'
  },
  {
    id: 4,
    name: 'Geladeira'
  },
  {
    id: 5,
    name: 'Outro'
  }
]

export const marcas = [
  {
    id: 1,
    name: 'Brastemp'
  },
  {
    id: 2,
    name: 'Consul'
  },
  {
    id: 3,
    name: 'Eletrolux'
  },
  {
    id: 4,
    name: 'Donizetti'
  },
  {
    id: 5,
    name: 'Outro'
  }
]
