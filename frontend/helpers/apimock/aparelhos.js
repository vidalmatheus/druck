import { aparelhos, tipos, marcas } from './db/aparelhos'
import { mockasync } from './mockutils'

export default {
  getAparelhosCadastrados () {
    return mockasync(aparelhos)
  },
  adcionaAparelho (aparelho) {
    return mockasync(aparelhos)
  },
  getTiposAparelhos () {
    return mockasync(tipos)
  },
  getMarcas () {
    return mockasync(marcas)
  }
}
